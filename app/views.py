from django.shortcuts import render, redirect
from .models import *
from .forms import *
# from .sol import *
from django.contrib.auth import login
from django.contrib.auth import logout, authenticate
from django.contrib.auth.decorators import login_required
import operator,string,random
from django.template import RequestContext


def index(requests):
    if requests.method == 'GET':
        print(len(Question.objects.all()[:10]))
        return render(requests, 'index.html', {
            'questions' : Question.objects.all()[:10]
        })
    else:
        return redirect('/error')


def signup(request):
    if request.method == 'POST':
        form = PersonSignUpForm(request.POST)
        if form.is_valid():

            if User.objects.filter(user_email=form.cleaned_data['email']).exists():
                return render(request, 'userExist.html')

            user = User()
            user.user_email = form.cleaned_data['email']
            user.user_wallet_address = form.cleaned_data['wallet_address']
            user.username = "".join(random.choice(string.ascii_letters + string.punctuation + string.digits) for x in range(random.randint(12, 12)))
            user.set_password(form.cleaned_data['password'])
            user.save()

        arg = {'username': form.cleaned_data['username']}

        return render(request, 'index.html', arg)
    else:
        form = PersonSignUpForm()
        arg = {'form': form}
        return render(request, 'signup.html', arg)


def loginses(requests):
    if requests.method == 'POST':
        form = PersonLoginForm(requests.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            user = authenticate(request=requests)
            if user is not None:
                login(requests, user)
                arg = {'email': email}

                return render(requests, 'questions.html', arg)
            print(user)
            return redirect('/error')
        print(form.errors)
        return redirect('/error')

    else:
        form = PersonLoginForm()
        arg = {'form': form}
        return render(requests, 'login.html', arg)


def logout(request):
    logout(request)
    return redirect('/')


def about(requests):
    if requests.method == 'GET':
        return render(requests, 'about.html')
    else:
        return redirect('/error')


@login_required
def ask_questions(request):
    if request.method == 'GET':
        return render(request, 'questions.html')

    elif request.method == 'POST':
        form = AskingQuestionForm(request.POST)
        if form.is_valid():
            question = form.cleaned_data['question']
            ether = form.cleaned_data['ether']
            expiration_time = form.cleaned_data['expiration_time']
            arg = {'text': question, 'ether': ether, 'expiration_date': expiration_time}
            q = Question(text='question', ether='ether', expiration_date='expiration_time')
            q.user = request.user
            cont_int = deploy_contract(q.user.user_wallet_address)
            money_for_questioning(cont_int, Question.user.user_wallet_address, Question.ether)
            return render(request, 'success.html', arg)
        else:
            return redirect('/error')

    else:
        return redirect('/error')

def pay_for_answers(requests):
    if Question.expiration_date == datetime.datetime.today().date():
        bounty_for_answering()

def contact(requests):
    if requests.method == 'GET':
        return render(requests, 'contact.html')
    else:
        return redirect('/error')

def notFound(requests):
    if requests.method == 'GET':
        return render(requests, '404.html')
    else:
        return redirect('/error')

def callSol(requests):
    if requests.method == 'GET':
        return render(requests, 'sol.py')
    else:
        return redirect('/error')