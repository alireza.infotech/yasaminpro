from django import forms
from app.models import *


class PersonSignUpForm(forms.Form):
        username = forms.CharField(max_length=50,required=True)
        email = forms.EmailField(max_length=30, required=True)
        wallet_address = forms.CharField(max_length=32, required=True)
        password = forms.CharField(max_length=30,required=True)

class PersonLoginForm(forms.Form):
        email = forms.EmailField(max_length=30, required=True)
        password = forms.CharField(max_length=15, required=True, widget=forms.PasswordInput)

class AskingQuestionForm(forms.Form):
        question = forms.CharField(max_length=1000,required=True)
        ether = forms.FloatField(required=True)
        expiration_time = forms.DateField(required=True)

class AnsweringQuestionForm(forms.Form):
        answer = forms.CharField(max_length=1000,required=True)
