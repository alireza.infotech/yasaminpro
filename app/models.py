from django.db import models
from django.contrib.auth.models import User


class MyUser(User):
    user_name = models.CharField(max_length=50)
    user_email = models.EmailField(max_length=30)
    user_wallet_address = models.CharField(max_length=32, blank=False, default='0x00000000000000')


class Answer(models.Model):
    text = models.CharField(max_length=255, blank=True)
    user = models.ForeignKey(MyUser, on_delete=models.DO_NOTHING, help_text='who answered the question')
    date = models.DateField(auto_now=True)

    def __str__(self):
        return self.text


class Question(models.Model):
    text = models.CharField(max_length=255, blank=True)
    title = models.CharField(max_length=255, blank=True)
    ether = models.FloatField(default=0)
    expiration_date = models.DateField()
    added_date = models.DateField(auto_now=True)
    user = models.ForeignKey(MyUser, on_delete=models.DO_NOTHING, help_text='who asked a question')
    answer = models.ManyToManyField(Answer, blank=True)

    def __str__(self):
        return str(self.id)+str(self.title)


class Rank(models.Model):
    user = models.ForeignKey(MyUser, on_delete=models.DO_NOTHING)
    answer = models.ForeignKey(Answer, on_delete=models.DO_NOTHING)
    rank = models.BooleanField(default=False, help_text='True is like and False is dislike')
    date = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.date) + ' : ' + str(self.answer)

    class Meta:
        unique_together = ('user', 'answer',)
