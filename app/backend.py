from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend


class EmailBackend(ModelBackend):
    def authenticate(self, requests, **kwargs):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(email=requests.POST['email'])

        except UserModel.DoesNotExist:
            return None

        else:
            if user.check_password(requests.POST['password']):
                return user

        return None